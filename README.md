Sen2Extract
===========

Sen2Extract is a R Shiny web application allowing to extract time series of spectral indices (previously calculated from Sentinel-2 L2A satellite images), for the requested locations and dates. This can only extract values from images already available in the associated database.

This application was initially developed by UMR ESPACE-DEV (IRD, Univ Antilles, Univ Guyane, Univ Montpellier, Univ Réunion) at University of La Réunion – SEAS-OI Station, in the frame of the S2-Malaria Project. This project, funded by CNES (TOSCA 2017-2020), aims at using satellite data provided by Sentinel-2 for epidemiological surveillance (intially for malaria). The main objective is to guarantee quick and easy access to these images and the resulting indices.

The original Sen2Extract application is available here: [Sen2Extract SEAS-OI](https://web.seas-oi.org/sen2extract/).
Sen2Extract does not allow to extract indices from any locality in the globe, but only on the Sentinel-2 L2A tiles available at SEAS-OI that have been produced in the framework of different projects.
See the About section for more information.



Contributing
------------

Scientific projects :

*  TOSCA S2-Malaria project, funded by CNES (TOSCA 2017-2020); 
*  INTERREG Renovrisk-impact project (2018-2020).

Development and improvment :

*  Victor Rechou ;
*  Pascal Mouquet ;
*  Didier Bouche ;
*  Sylvaine Jégo ;
*  Florian Girond.


Conceptualization and Coordination :

*  Christophe Révillion ;
*  Pascal Mouquet ;
*  Vincent Herbreteau.

![Espace-Dev](../docs/source/esdev.png)


This project is open to anyone willing to help. You can contribute by adding new features, reporting and fixing bugs, or propose new ideas.

Funding
-------

![Funded by CNES and INTERREG V](../docs/source/funding.png)

License
-------

GPLv3+
