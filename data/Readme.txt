____________________________

  SEN2EXTRACT APPLICATION
____________________________

Sen2extract is a shiny application allowing you to extract time series of spectral indexes on your study sites.
This application was developed by the UMR ESPACE-DEV, within the S2-Malaria project, 
a project funded by CNES and aimed at using satellite data provided by Sentinel-2 for malaria surveillance.

  ~~~~ LINK TO THE APPLICATION ~~~~
https://web.seas-oi.org/sen2extract/

  ~~~~ PROCESS ~~~~
This data comes from the import of a zipped shapefile. This shapefile has been transformed into WGS84. 
The user has chosen to extract one or more available indices (NDVI, NDWIGAO, NDWIMCF, MNDWI) over a specific period. 
The result was sent to his email address in a zipped file containing the CSV(s) and this Readme.txt.
The treatments have been realised on the servers of the SEAS-OI station.

  ~~~~ RESULTS ~~~~
list of files present in the zip :
	1- csv(s) named like this : [Name of the layer][_WGS84][date 1][date 2].csv
	2- Readme.txt

  ~~~~ OUTPUT VARIABLES ~~~~
- date: Acquisition date and time UT
- fid: Id of the shapfile entity
- tile: Tile name
- filename: Name of the file used for extraction
- count: Number of pixels used to compute statistics
- nodata: Number of pixels without data. count + nodata should be constant for an entity
- nbcld: Number of pixels detected as clouds. They are considered as nodata.
- cldprb: Mean cloud probabiliy of the counted pixels 
- min: Min pixel value in the counted pixels
- max: Max pixel value in the counted pixels
- mean: Mean value of the counted pixels
- std: Standard deviation of the counted pixels
- median: Median value of the counted pixels
- percentile_25: Percentile 25 value of the counted pixels
- percentile_75: Percentile 75 value of the counted pixels
- + all fields from the attribute table of the inuput shapefile


If you receive an empty archive file, you either:
- selected the wrong indice (check available indices)
- selected a wrong time period (check available dates)
- or something else happened preventing the extraction to finish (please contact us)

______SEAS-OI - 2020______
