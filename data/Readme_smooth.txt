

____________________________

  SEN2LISS APPLICATION
____________________________

Sen2liss is an option that allows to smooth an extract time series of spectral index for each polygon.
This option was developed by the UMR ESPACE-DEV, within the S2-Malaria project, 
a project funded by CNES and aimed at using satellite data provided by Sentinel-2 for malaria surveillance.

  ~~~~ LINK TO THE APPLICATION ~~~~
http://indices.seas-oi.org/sen2extract/

  ~~~~ PROCESS ~~~~
The database in the input file is the CSV file obtained by Sen2extrat for each index. Only extractions from polygons can have a smoothing on their time series.
First of all, a data quality study is carried out (removing dates with more than 75% cloud cover & deleting dates with a cloud detection problem).
Then the smoothing model is performed on there maining dates.
Three models are achieved based on spline model with the mean index:
	- Cubic spline: only on observed dates
	- Fourier transform and B-spline: need imputed data on missing dates with a regular time step and at least 3 years of temporality
Finally, the smoothing is performed on all dates between the first and the last observed dates. We have a mean index smoothed on all dates.
If there are several polygons, the first (resp. last) date is the first (resp. last) observed date on the set of polygons.
The result is sent to the user's email address in a zipped file containing all the CSVs for each index (extraction and smoothing) and two text files ("Readme.txt" for extraction and "Readme_smooth.txt" for smoothing).
If there is no CSV file for smoothing, it means that there is not enough data to perform a smoothing model.
The treatments have been realised on the servers of the SEAS-OI station.

  ~~~~ RESULTS ~~~~
List of variables included in the CSV file for smoothing:
	1 - csv(s) file named: [Name of the layer][_WGS84][date 1][date 2]_smooth.csv
		- fid: polygon ID
		- date2: smoothing date
		- index: index name
		- cubic.spline: mean index smoothed with the Cubic-spline method
		- fourier: mean index smoothed with Fourier transform method (only if there is more than 3 years of temporality)
		- seuil.F: ripple threshold for the Fourier transform method (in percent) (only if there is more than 3 years of temporality)
		- b.spline: mean index smoothed with B-spline method (only if there is more than 3 years of temporality)
		- seuil.B: ripple threshold for the B-spline method (in percent) (only if there is more than 3 years of temporality)
		- ... variables obtained in the CSV file in input (only on observed dates)
		- ... variables obtained in the shapefile
		- pct.na: NAs on the polygon (in percent)
		- ratio.std: indicator about cloud detection problem (1: there is a problem; 0: no problem) (only on observed dates)
		- raw.data: raw data mean index used to perform the Cubic-spline model (only on observed dates)
		- don.imp: imputed mean index used to perform the Fourier transform and the B-spline models (usually every 5 days)

	2 - Readme_smooth.txt

______SEAS-OI - 2020______
