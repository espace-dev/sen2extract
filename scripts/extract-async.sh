    #-------- WRITE README.TXT --------#  
    Readme <- file("Readme.txt", "w")
    writeLines("____________________________", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("  SEN2EXTRACT APPLICATION", Readme, sep = "\r\n")
    writeLines("____________________________", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("Sen2extract is a shiny application allowing you to extract time series of spectral indexes on your study sites.", Readme, sep = "\r\n")
    writeLines("This application was developed by the UMR ESPACE-DEV, within the S2-Malaria project, ", Readme, sep = "\r\n")
    writeLines("a project funded by CNES and aimed at using satellite data provided by Sentinel-2 for malaria surveillance.", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("  ~~~~ LINK TO THE APPLICATION ~~~~", Readme, sep = "\r\n")
    writeLines("http://indices.seas-oi.org/sen2extract/", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("  ~~~~ PROCESS ~~~~", Readme, sep = "\r\n")
    writeLines("This data comes from the import of a zipped shapefile. This shapefile has been transformed into WGS84. ", Readme, sep = "\r\n")
    writeLines("The user has chosen to extract one or more available indices (NDVI, NDWIGAO, NDWIMCF, MNDWI) over a specific period. ", Readme, sep = "\r\n")
    writeLines("The result was sent to his email address in a zipped file containing the CSV(s) and this Readme.txt.", Readme, sep = "\r\n")
    writeLines("The treatments have been realised on the servers of the SEAS-OI station.", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("  ~~~~ RESULTS ~~~~", Readme, sep = "\r\n")
    writeLines("list of files present in the zip :", Readme, sep = "\r\n")
    writeLines("	1- csv(s) named like this : [Name of the layer][_WGS84][date 1][date 2].csv", Readme, sep = "\r\n")
    writeLines("	2- Readme.txt", Readme, sep = "\r\n")
#    writeLines("	3- shape_path.csv", Readme, sep = "\r\n")
    writeLines("", Readme, sep = "\r\n")
    writeLines("______SEAS-OI - 2019______", Readme, sep = "\r\n")
    close(Readme)
    

    #-------- DISPLAY AN ALERT --------#  
    shinyalert("Your request is being processed. Please keep this window opened.\n\n You will soon receive a link to download the extracted data.",
               type = "success", showConfirmButton = FALSE)
    
    #-------- SYSTEM FUNCTION --------#
    sys <- paste('/home/webdev/shiny/appli/sen2extract/extract_timeseries_webdev.sh ',
                 shpWGS$V1,' ',
                 paste(input$indice, collapse = ','), ' ',
                 input$dates[1], ' ',
                 input$dates[2],
                 sep = "")
    print(sys)
    system(sys)

#####################################################################################################################################
#####################################################################################################################################

#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>>/home/webdev/logs/sen2extract.log 2>&1

echo ""
echo "***** start `date` *****"
[ $# -eq 0 ] && { echo "Usage: $0 /path/to/file.shp indice1[,indice2[,indice3]] start_date(AAAA-MM-DD) end_date(AAAA-MM-DD)"; exit 1; }

tmp_folder=$(ssh webdev@sentinel-prod-seas-oi "mktemp -d -p /DATA_S2/S2_SEN2EXTRACT/TEMP/")
ssh sentinel-prod-seas-oi "chmod 777 $tmp_folder"
scp `find $(dirname $1) -name "$(basename ${1%shp})*"` webdev@sentinel-prod-seas-oi:${tmp_folder}
tmp_shp=$tmp_folder'/'$(basename $1)
ssh sentinel-prod-seas-oi "sudo -i -u sentinel /usr/bin/python3 /usr/local/S2_SEN2EXTRACT/lance_sen2chain_time_series.py $tmp_shp $2 $3 $4"
scp webdev@sentinel-prod-seas-oi:${tmp_folder}/$(basename ${1%.shp})/*.csv $(dirname $1)
ssh webdev@sentinel-prod-seas-oi "rm -dr $tmp_folder"

#####################################################################################################################################
#####################################################################################################################################

    #-------- ZIP ALL CSV --------#
    output_zip <- paste(sub('\\.zip$', '', input$zip$name), "_Sen2Extract_", format(Sys.time(), "%Y%m%d%H%M%S"), ".zip", sep="")
    
    Zip_Files <- list.files(path = getwd(), pattern = ".csv$|.txt$")
    zip(zipfile = output_zip, files = Zip_Files)
    do.call(file.remove, list(Zip_Files))
    
    #-------- MOVE ZIP --------#
    split_path <- function(path) {
      if (dirname(path) %in% c(".", path)) return(basename(path))
      return(c(basename(path), split_path(dirname(path))))
    }

    wd <- getwd()
    p = split_path(wd)
    outputfolder = file.path("/home/webdev/shiny/appli/sen2extract/www/extractions/", p[2], p[1])
    dir.create(outputfolder, recursive=TRUE, showWarnings = FALSE)
    file.copy(output_zip, outputfolder, overwrite = TRUE )
    if (Sys.info()["nodename"] == "indices-seas-oi") {
    	host = "indices.seas-oi.org"
    } else if (Sys.info()["nodename"] == "indices-dev-seas-oi") {
    	host = "indices-dev-seas-oi:3838"
    } else {
    	host = "server_XXX"
    }
    http = paste("http://", host, "/sen2extract/extractions/", p[2], "/", p[1], "/", output_zip, sep="")

	#-------- DIRECT LINK DOWNLOAD --------#
    shinyalert(paste("<a href=\"", http, "\">", "Direct link", "</a>", " to your extracted data", sep=""), html = TRUE, type = "success")

    #-------- EMAIL FUNCTION --------#
    sender <- "support@seas-oi.org"
    recipients <- input$mail
    email <- send.mail(from = sender,
                       to = recipients,
                       subject="[Sen2Extract] Your Sen2Extract query",
                       body = paste("Dear user,\n\n", 
									"Please find below the link (valid 7 days) to download the zipped csv file(s) corresponding to your spatiotemporal query on Sentinel-2 indices, ",
									"using Sen2Extract web application.\n",
									http, "\n\n",
									"You can now return to Sen2Extract web application to view the values of this csv in a table or graph (http://indices.seas-oi.org/sen2extract/).\n\n",
									"We remind you that this service is available free of charge, based on the ESA Copernicus Sentinel-2 images, ",
									"processed at SEAS-OI Station in the frame of two projects : S2-Malaria Project (CNES TOSCA 2017-2019) and Renovrisk-impact Project (INTERREG 2018-2020).\n\n",
									"Cheers,\n",
									"Sen2Extract Team",
									sep=""),
#                       attach.files = dir(pattern = output_zip),
                       smtp = list(host.name = "mail.gandi.net", port = 587,
                                   user.name = "support@seas-oi.org",
                                   passwd ="se@s-oi974", ssl = TRUE),
                       authenticate = TRUE,
                       send = TRUE)
    runjs("$('.busy').hide();")
    Sys.sleep(2)
  }, ignoreInit = TRUE)

  
  #-------- PLOT TAB --------#
  output$contents <- renderDataTable({
    inFile <- input$csv_tab
    if (is.null(inFile))
      return(NULL)
    read.csv(inFile$datapath)
  }, options = list(scrollX = TRUE))

  
  #-------- REACTIVE DATA --------#
  data <- reactive({
    req(input$csv_chart)
    infile <- input$csv_chart
    if (is.null(infile))
      return(NULL)
    df <- read_csv(infile$datapath)
    updateSelectInput(session, inputId = 'X', label = 'Field X:',
                      choices = names(df), selected = names(df)[1])
    updateSelectInput(session, inputId = 'Y', label = 'Field Y:',
                      choices = names(df), selected = names(df)[11])
    updateSelectInput(session, inputId = 'group', label = 'Group by:',
                      choices = names(df), selected = names(df)[16])
    return(df)
  })
  

  #-------- PLOT CHART --------#
  output$plot <- renderPlotly({
    ggplot(data()) +
      geom_line(mapping = aes(x = get(input$X), y = get(input$Y), color = as.character(get(input$group)))) +
      labs(x = "", y = input$Y, title = "Index Values")
  })
  
  
  #-------- RESET BUTTONS --------#
  observeEvent(input$reset_button, {
    shinyjs::js$reset()
  })
  
  
  observeEvent(input$reset_button2, {
    reset("csv")
  })
})


