#!/bin/bash

# cf. logrotate
# supprimer les vieux logs +10j
#~ find /home/webdev/shiny/logs -type f -mtime +10 -name "sen2extract-webdev-*" -exec rm -r {} \;

# supprimer les anciennes extractions +10j
find /home/webdev/shiny/appli/sen2extract/www/extractions/ -type d -ctime +10 -name "Rtmp*" | xargs rm -rf
