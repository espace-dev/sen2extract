#!/bin/bash
#~ exec 3>&1 4>&2
#~ trap 'exec 2>&4 1>&3' 0 1 2 3
#~ exec 1>>/home/webdev/logs/sen2extract.log 2>&1

echo ""
echo "***** start `date` *****"
[ $# -eq 0 ] && { echo "Usage: $0 /path/to/file.shp indice1[,indice2[,indice3]] start_date(AAAA-MM-DD) end_date(AAAA-MM-DD) user@email.com TRUE/FALSE(smoothing)"; exit 1; }

tmp_folder=$(ssh webdev@sentinel-prod-seas-oi "mktemp -d -p /DATA/S2/PRODUCTS/EXTRACTION/SEN2EXTRACT/TEMP/")
ssh sentinel-prod-seas-oi "chmod 777 $tmp_folder"
scp `find $(dirname $1) -name "$(basename ${1%shp})*"` webdev@sentinel-prod-seas-oi:${tmp_folder}
tmp_shp=$tmp_folder'/'$(basename $1)
ssh webdev@sentinel-prod-seas-oi "sudo -i -u sentinel /usr/bin/python3 /usr/local/scripts/webdev/S2_SEN2EXTRACT/lance_sen2chain_time_series.py $tmp_shp $2 $3 $4"
scp webdev@sentinel-prod-seas-oi:${tmp_folder}/$(basename ${1%.shp})/*.csv $(dirname $1)
ssh webdev@sentinel-prod-seas-oi "rm -dr $tmp_folder"

www_folder=/home/webdev/shiny/appli/sen2extract/www/extractions/$(basename $(dirname $1))/
mkdir -p $www_folder

# Lissage
if $6; then
   /home/webdev/sen2liss/sen2liss/Automatisation_lissage_Sen2Extract.r $(dirname $1)
   cp /home/webdev/shiny/appli/sen2extract/data/Readme_smooth.txt $www_folder
fi

cp /home/webdev/shiny/appli/sen2extract/data/Readme.txt $www_folder
mv $(dirname $1)/*.csv $www_folder
zip_name=$(basename $1 .shp)_Sen2Extract_$(date +%Y%m%d%H%M%S).zip
zip -jDmr $www_folder/$zip_name $www_folder -i '*.csv' -i '*.txt' 
case $HOSTNAME in
  ("indices-dev-seas-oi") base="http://indices-dev-seas-oi:3838";;
  ("indices-seas-oi") base="https://web.seas-oi.org";;
  ("indices2-seas-oi") base="http://indices2-seas-oi:3838";;
esac
http=$base/sen2extract/extractions/$(basename $(dirname $1))/$zip_name
body="<html><head></head><body>Dear user,<br><br>"\
"Please find below the link (valid 7 days) to download the zipped csv file(s) corresponding to your spatiotemporal query on Sentinel-2 indices, "\
"using Sen2Extract web application.<br><a href="$http">"$http"</a><br><br>You can now return to Sen2Extract web application to view the values of this csv in a table or graph"\
"(http://indices.seas-oi.org/sen2extract/).<br><br>"\
"We remind you that this service is available free of charge, "\
"based on the ESA Copernicus Sentinel-2 images, processed at SEAS-OI Station in the frame of two projects : <br>"\
"S2-Malaria Project (CNES TOSCA 2017-2019) and Renovrisk-impact Project (INTERREG 2018-2020).<br><br>"\
"Cheers,<br>Sen2Extract Team</body></html>"
export REPLYTO="support@seas-oi.org"
echo $body | mutt -s "[Sen2Extract] Your Sen2Extract query" $5
#~ rm -r $(dirname $1)
