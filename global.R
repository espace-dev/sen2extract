library(shiny)
library(shinydashboard)
library(leaflet)
library(shinyjs)
library(V8)
library(shinyalert)
library(leaflet.extras)
library(DT)
library(tidyverse)
library(plotly)
library(shinyBS)
library(arrow)
library(sfarrow)
library(sf)
library(dplyr)

shapeTiles <- st_drop_geometry(st_read("/home/webdev/shiny/appli/sen2extract/data/db_total_shp.shp"))
# If the index becomes a non-spatial tabular file,
# replace with a simple read.csv

shapeTiles <- open_dataset("data/sentinel-2_grid.parquet") %>%
    filter(Name %in% shapeTiles$Name) %>%
    read_sf_dataset() %>%
    merge(.,shapeTiles)

css <- "
.busy {
  position: fixed;
  z-index: 1000;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: none;
  background-color: rgba(255, 255, 255);
  text-align: center;
  border-radius: 50px;
  border-color: rgba(0,0,0);
  border-style: solid;
}"